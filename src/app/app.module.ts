import { AppComponent } from './app.component';
import { AuthService } from './auth/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { EndpointService } from './endpoint.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { RegisterService } from './register/register.service';
import { SharedModule } from './shared/shared.module';
import { TokenInterceptor } from './interceptors/token.interceptor';

import { CalendarioModule } from './calendario/calendario.module';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LogoutComponent } from './logout/logout.component';
import { CompareDirective } from './utils/compare.directive';
import { RegisterSuccessfulComponent } from './register/register-successful/register-successful.component';



@NgModule({
  declarations: [
    AppComponent,
    CompareDirective,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,    
    RegisterSuccessfulComponent    
  ],
  imports: [
    BrowserModule,
    CalendarioModule,
    NgbModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule
  ],
  exports: [],
  providers: [AuthService,RegisterService,EndpointService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
