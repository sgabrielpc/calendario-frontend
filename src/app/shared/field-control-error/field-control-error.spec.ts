import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFieldControlErrorComponent } from './form-field-control-error.component';

describe('FormFieldControlErrorComponent', () => {
  let component: FormFieldControlErrorComponent;
  let fixture: ComponentFixture<FormFieldControlErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFieldControlErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFieldControlErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
