import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'field-control-error',
  templateUrl: './field-control-error.template.html',
  styleUrls: ['./field-control-error.css']
})
export class FieldControlErrorComponent implements OnInit {

  @Input() errorMsg: string;
  @Input() showError: boolean;

  constructor() { }

  ngOnInit() {
  }
  

}
