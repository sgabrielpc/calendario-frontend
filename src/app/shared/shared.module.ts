import { NgModule } from '@angular/core';
import { FieldControlErrorComponent } from './field-control-error/field-control-error.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [    
    CommonModule
  ],
  declarations: [
    FieldControlErrorComponent
  ],
  exports: [FieldControlErrorComponent],
  providers: []
})

export class SharedModule {}
