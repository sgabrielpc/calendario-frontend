import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import {FormGroup, FormBuilder, Validators} from '@angular/forms'

import { User } from '../shared/user';
import { invalid } from 'moment';
import { AuthService } from '../auth/auth.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-login',
  templateUrl: './login.template.html',
  styleUrls: ['./login.css']
})
export class LoginComponent implements OnInit {
  
  public invalidLogin: boolean = false;
  public serverNotResponding: boolean = false;
  public form: FormGroup;
 

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {

      this.authService.logout();

      this.form = this.formBuilder.group({
        username: [null, [Validators.required]],
        password: [null, Validators.required]
      })            
  }

  submitLogin() {
    
    // Verifica se algum dos campos eh invalido, se for, marca como touched para o CSS vermelho ser aplicado
    if (this.form.get('username').invalid) {
      this.form.get('username').markAsTouched();      
    }
    if (this.form.get('password').invalid) {
      this.form.get('password').markAsTouched();      
    }

    if (this.form.valid) { // So loga se o formulario for valido

      // Obtem os dados preenchidos do formulario e armazena na variavel que passaremos por post
      let user: User = new User(null,null,this.form.get('username').value,this.form.get('password').value);
            
      // POST login
      this.authService.authenticate(user);
      
      this.authService.getLoginChange().subscribe(
        // Caso de sucesso
        (err) => {  
          if (err == null) {               
            this.router.navigate(['/']); // Redireciona para a pagina inicial
          } else {                        
            if (err.error instanceof Error) {     // Se for um erro do servidor           
              this.serverNotResponding = true;    // Exibe mensagem de erro de "dificuldades tecnicas"
            } else {
              if (err.status == 0) {
                this.serverNotResponding = true;
                this.invalidLogin = false;
              } else {
                this.invalidLogin = true;         // Exibe mensagem de erro de "usuario / senha invalidos"
                this.serverNotResponding = false;
              }            
            }
          }
        });        
    } 
  }

  verifyValidTouched(field) {
    return !this.form.get(field).valid &&  this.form.get(field).touched;
  }

  applyCssError(field) {
    return {
      'has-error': this.verifyValidTouched(field),
      'has-feedback': this.verifyValidTouched(field)
    }
  }


}