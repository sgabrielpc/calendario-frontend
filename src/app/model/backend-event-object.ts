

export interface BackendEventObject {
  id: string;  
  start: string;
  end: string;
  title: string;
  primaryColor: string;  
  secondaryColor: string;  
  allDay: boolean;
  resizable: boolean;
  draggable: boolean;
  user: string;
}