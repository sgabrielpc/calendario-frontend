import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { RegisterService } from '../register/register.service';

@Injectable()
export class RegisterGuard implements CanActivate {

  constructor(
    private registerService: RegisterService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<boolean> | boolean {    
      if (this.registerService.registerSubmitted) {
          this.registerService.registerSubmitted = false;
          return true;
      }
      this.router.navigate(["/register"]);
    return false;
  }
}