import { EndpointService } from './../endpoint.service';
import { Injectable } from '@angular/core';
import { HttpRequest,HttpHandler,HttpEvent,HttpInterceptor} from '@angular/common/http';
//import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private endpointService: EndpointService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(request);        
    if (request.url != this.endpointService.getUrl("register") 
    && request.url != this.endpointService.getUrl("login") 
    && request.url != this.endpointService.getUrl("userExists")) {            
    
     request = request.clone(        
     {
      setHeaders: {        
          Authorization: `Bearer ${localStorage.getItem('access_token')}`        
        }
    });
    } else {
        request = request.clone();
    }
    return next.handle(request);
  }
}