import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {  HttpResponse, HttpErrorResponse , HttpRequest,HttpHandler,HttpEvent,HttpInterceptor} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    return next.handle(request).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', err.error.message);                    
           } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${err.status}, body was: ${err.error}`);                   
          }
          // Caso o token tenha expirado, redirecionar pro login
          if (err.status === 401) {    
              console.log("Token expired")   ;                                 
              this.router.navigate(["/login"]);
          }
          if (err.status === 0) {    
            console.log("Server not found: ")   ;                 
        }
        }
      });
  }
}