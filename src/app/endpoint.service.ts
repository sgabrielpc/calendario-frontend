import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class EndpointService {

  constructor() { }

    // Retorna o `path` formatado junto com o domínio
    getUrl(url: string): string {      
      return environment.baseUrl + url;
    }
  

}
