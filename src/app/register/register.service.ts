import { EndpointService } from './../endpoint.service';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { User } from '../shared/user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class RegisterService {

  public registerSuccessful = new Subject();
  public allowSuccessPageAccess = new Subject();
  public registerSubmitted: boolean = false;
  
  constructor(private http: HttpClient, 
              private endpointService: EndpointService) { }

  public register(user: User) : void {
      let httpResponse =  this.http.post(this.endpointService.getUrl("register"),JSON.stringify(user)).subscribe(
        (data) => {            
          this.registerSuccessful.next(data['message']);
          this.registerSubmitted = true;          
        }  
      );
  }

  public userExists(email: string) : Observable<any> {
    return this.http.post(this.endpointService.getUrl("userExists"),JSON.stringify({"username": email}));
  }
}
