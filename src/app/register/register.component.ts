import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { User } from '../shared/user';
import { RegisterService } from './register.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-register',
  templateUrl: './register.template.html',
  styleUrls: ['./register.css']
})
export class RegisterComponent implements OnInit {
  register: FormGroup;  
  public _authService: AuthService;    
  public showErrorMessage: boolean = false;  
  
  constructor(private frmBuilder: FormBuilder,
  private registerService: RegisterService,
  private authService: AuthService,
  private router: Router) {
    this._authService = authService;
   }
  
  ngOnInit(){    
    localStorage.setItem('registerSuccessful',"false");

    this.register = this.frmBuilder.group({
      firstName:[null, [Validators.required, Validators.minLength(3)]],
      lastName:[null, [Validators.required, Validators.minLength(3)]],
      email:[null, [Validators.required]],      
      password:[null, [Validators.required, Validators.minLength(6),]],
      verify:[null, [Validators.required]]
    });
  }
  
  get firstName() { return this.register.get('firstName'); }
  get lastName() { return this.register.get('lastName'); }
  get email() { return this.register.get('email'); }  
  get password() { return this.register.get('password'); }
  get verify() { return this.register.get('verify'); }
 
  public submitNewUser() { 
    
    if (this.register.valid) {
      let user: User = new User(
        this.register.get('firstName').value,
        this.register.get('lastName').value,
        this.register.get('email').value,
        this.register.get('password').value        
      );
      
      
      this.registerService.register(user);
      this.registerService.registerSuccessful.subscribe(serverMessage => { 
        if (serverMessage == 'OK') {                                  
          this.router.navigate(["/register/success"]);          
        } else {          
          this.showErrorMessage = true;
          console.log("serverMessage: " + serverMessage);
        }
      }); 
    } else {
      console.log("Form Invalido");
      console.log(this.register);
    }    
  }

  checkUserExists() {
      
      this.registerService.userExists(this.register.get('email').value).subscribe(
        response => {          
          if (response['message'] == true) {
              let errors = this.register.get('email').errors;
              if (errors == null) {
                this.register.get('email').setErrors(
                  {
                    "notUnique": true
                  }
                );
              } else {
                errors['notUnique'] = true;
                this.register.get('email').setErrors(errors);
              }
          } 
        } 
      );
  }
}

