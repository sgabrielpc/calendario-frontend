import { colors } from './colors';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  NgbDatepickerModule,
  NgbTimepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { DateTimePickerComponent } from './date-time-picker.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyCustomEventTitlePipe } from './my-custom-event-title.pipe'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    NgbDatepickerModule.forRoot(),
    NgbTimepickerModule.forRoot(),    
  ],
  declarations: [DateTimePickerComponent,MyCustomEventTitlePipe],
  exports: [DateTimePickerComponent,MyCustomEventTitlePipe]
})
export class UtilsModule {}
