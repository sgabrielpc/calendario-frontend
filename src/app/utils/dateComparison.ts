import { CalendarEvent } from 'calendar-utils';
export class DateComparison {
    start: number;
    end: number;
    duration: number;
    constructor(start: Date, end: Date) {
        this.start = start.getTime();
        this.end = end.getTime();        
    }

    public static isInBetween(numberToCompare: number, startTime: number, endTime: number) {
        //console.log(numberToCompare + " >=  " + startTime + "(" +  (numberToCompare >= startTime) + ")" + " &&  " + numberToCompare + " <= " + endTime + "(" +  (numberToCompare <= endTime) + ")" + ": " +  (numberToCompare >= startTime && numberToCompare <= endTime));
        return (numberToCompare >= startTime && numberToCompare <= endTime);
      }
    
      public static isInConflict(_event1: CalendarEvent, _event2: CalendarEvent) : boolean {    
    
        if (_event1.id == _event2.id) {
            console.log("events with same ID comparison");
            return false;
        }
        let event1: DateComparison = new DateComparison(_event1.start,_event1.end);
        let event2: DateComparison = new DateComparison(_event2.start,_event2.end);                                
    
        /*  [event2.start]   [event1.start]    [event1.end]  [event2.end] */
        /*  [event1.start]   [event2.start]    [event2.end]  [event1.end] */
    
        /*  [event1.start]   [event2.start]    [event1.end]  [event2.end] */
        /*  [event2.start]   [event1.start]    [event2.end]  [event1.end] */
        if ( (this.isInBetween(event1.start,event2.start,event2.end))
           || (this.isInBetween(event1.end,event2.start,event2.end))
            || (this.isInBetween(event2.start,event1.start,event1.end))
            || (this.isInBetween(event2.end,event1.start,event1.end))      
        ) {
            return true;
        }
        return false;
      }
}