import { RegisterGuard } from './guards/register.guard';
import { RegisterSuccessfulComponent } from './register/register-successful/register-successful.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';  
import { CalendarioComponent } from './calendario/calendario.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: ''          , component: CalendarioComponent },		
  { path: 'login'     , component: LoginComponent },	
  { path: 'logout'     , component: LogoutComponent, 
    canActivate: [AuthGuard]
  },
  {path: 'register/success', component: RegisterSuccessfulComponent,
  canActivate: [RegisterGuard]},
  { path: 'register'  , component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard,RegisterGuard]
})
export class AppRoutingModule { }
