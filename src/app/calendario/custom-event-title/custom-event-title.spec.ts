import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomEventTitleComponent } from './custom-event-title.component';

describe('CustomEventTitleComponent', () => {
  let component: CustomEventTitleComponent;
  let fixture: ComponentFixture<CustomEventTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomEventTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomEventTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
