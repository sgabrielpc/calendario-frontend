import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'calendar-header-component',
  templateUrl: './header.template.html',
  styleUrls: ['./header.css']
})
export class HeaderComponent implements OnInit {

  @Input() view: string;

  @Input() viewDate: Date;

  @Input() locale: string = 'en';

  @Output() viewChange: EventEmitter<string> = new EventEmitter();

  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
