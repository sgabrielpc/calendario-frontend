import { BackendEventObject } from './../model/backend-event-object';
import { EndpointService } from './../endpoint.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/';
import { Injectable } from '@angular/core';
import {startOfDay,endOfDay,subDays,addDays,endOfMonth,isSameDay,isSameMonth,addHours,addMinutes} from 'date-fns';
import {CalendarEvent,CalendarEventAction,CalendarEventTimesChangedEvent} from 'angular-calendar';
import { colors} from "../utils/colors"
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { EventEmitter } from 'events';


@Injectable()
export class CalendarioService {
  
  public dateChanged : Subject<CalendarEvent> = new Subject<CalendarEvent>();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        //this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        //this.events = this.events.filter(iEvent => iEvent !== event);
        //this.handleEvent('Deleted', event);
      }
    }
  ];

  constructor(private http: HttpClient,
              private endpointService: EndpointService) { }
  
  getEvents(): Observable<any> {

    return this.http.get<BackendEventObject[]>(this.endpointService.getUrl("getEvents")).map(
      receivedEvents => receivedEvents.map(element => ({
            id: element.id,
            start: new Date(element.start),
            end: (element.end == null? undefined : new Date(element.end)),
            title: element.title,
            color: {
              primary: element.primaryColor,
              secondary: element.secondaryColor
            },
            actions: this.actions,
            dateIsValid: true
          }))
    );    
  }

  

addEvents(events): Observable<any> {        
  
    events.forEach(element => {
      element.start = element.start.getTime();      
      if (element.end != undefined)
        element.end = element.end.getTime();      
    });        

    return this.http.post(this.endpointService.getUrl("addEvents"),JSON.stringify(events));    
  }

  updateEvents(eventsToUpdate) : Observable<any>  {

    eventsToUpdate.forEach(element => {
      element.start = element.start.getTime();      
      if (element.end != undefined)
      element.end = element.end.getTime();      
    })
    
    return this.http.post(this.endpointService.getUrl("updateEvents"),JSON.stringify(eventsToUpdate));  
  }

}



 /*return [
      {
        start: subDays(addMinutes(addHours(startOfDay(new Date()),13),30), 1),  
        end: addDays(new Date(), 1),
        title: 'A 3 day event',
        color: colors.red,
        actions: this.actions
      },
      {
        start: startOfDay(new Date()),
        title: 'An event with no end date',
        color: colors.yellow,
        actions: this.actions
      },
      {
        start: subDays(endOfMonth(new Date()), 3),
        end: addDays(endOfMonth(new Date()), 3),
        title: 'A long event that spans 2 months',
        color: colors.blue
      },
      {
          start: addHours(startOfDay(new Date()), 2),
          end: new Date(),
          title: 'A draggable and resizable event',
          color: colors.yellow,
          actions: this.actions,
          resizable: {
            beforeStart: true,
            afterEnd: true
          },
          draggable: true
      }
    ];*/