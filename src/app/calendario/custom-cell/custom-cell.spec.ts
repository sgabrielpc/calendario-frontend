import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthTemplateComponent } from './month-template.component';

describe('MonthTemplateComponent', () => {
  let component: MonthTemplateComponent;
  let fixture: ComponentFixture<MonthTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
