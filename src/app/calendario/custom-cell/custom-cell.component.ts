import { isSameDay, isSameMonth } from 'date-fns';
import { CalendarEvent } from 'angular-calendar';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter, TemplateRef } from '@angular/core';

@Component({
  selector: 'custom-cell',
  templateUrl: './custom-cell.template.html',
  styleUrls: ['./custom-cell.css']
})
export class CustomCellComponent implements OnInit {
  @ViewChild('templateVar') templateVar: TemplateRef<any>;  // Referencia a variavel ng-template no .html desse componente, sera emitido para o componente principal, usando o templateVarEmitter
  @Output() onMouseOverEmitter = new EventEmitter();        // Indica ao componente principal, se o mouse foi passado por cima de um dia, e qual
  @Output()templateVarEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() {    
    this.templateVarEmitter.emit(this.templateVar);
  }

  /*******************************************************************************************************************************************************************
   * Metodo: onMouseOver                                                                                                                                             *
   * Descricao : Emite um evento para o componente principal, dizendo que o mouse foi passado por cima de um dia,enviando o dia e os eventos daquele dia, no evento  *
   * Argumentos: {date, events} = Eh um objeto do tipo 'day' que tem .date que te da a data completa do evento, e o .event que te retorna uma lista de eventos       *
   * Retorno   : nenhum                                                                                                                                              *
   * ****************************************************************************************************************************************************************/
  onMouseOver({ date, events }: { date: Date; events: CalendarEvent[] }): void {              
    this.onMouseOverEmitter.emit({ date, events });
  }    
}
