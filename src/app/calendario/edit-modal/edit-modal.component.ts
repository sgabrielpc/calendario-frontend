import { DateComparison } from './../../utils/dateComparison';
import { AuthService } from './../../auth/auth.service';
import { colors } from '../../utils/colors';
import { CalendarEvent } from 'angular-calendar';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { startOfDay, endOfDay } from 'date-fns';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { CalendarioService } from '../calendario.service';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Component({
  selector: 'edit-modal-component',
  templateUrl: './edit-modal.template.html',
  styleUrls: ['./edit-modal.css']
})
export class EditModalComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;    
  @Output() refreshEmitter = new EventEmitter;
  @Output() addEventEmitter = new EventEmitter;
  @Output() deleteEventEmitter = new EventEmitter;


  @Input() viewDate: Date;

  public newEvents: CalendarEvent[];
  public eventsToDisplay : CalendarEvent[];
  public originalEventsToDisplay : CalendarEvent[];
  public showNewEventMenu: boolean = false;
  public modalRef: NgbModalRef;
  public showErrorDiv: boolean = false;
  public errorMessage: string;  


  constructor(private modal: NgbModal,
  private calendarioService: CalendarioService,
  private authService: AuthService) { }
  
  ngOnInit() {
  }



  // Abre o modal e edicai de eventos, exibindo os eventos daquele dia, fornecido como parametro pelo calendar.component, no metodo dayClicked
  public openEditModal(eventsToDisplay: CalendarEvent[], allEvents: CalendarEvent[]): void {
    this.newEvents = [];
    this.eventsToDisplay = eventsToDisplay;
    this.showErrorDiv = false;    
    this.originalEventsToDisplay = JSON.parse(JSON.stringify(this.eventsToDisplay));

    this.calendarioService.dateChanged.subscribe(changedDateEvent => {
      
      //changedDateEvent.dateIsValid = true;        
      //this.showErrorDiv = false;     
      this.errorMessage = ""; 
      this.showErrorDiv = false; 

      if (changedDateEvent.end.getTime() < changedDateEvent.start.getTime()) {
        changedDateEvent.dateIsValid = false;
        this.errorMessage = "The end date cannot be before the start date"
        this.showErrorDiv = true;
      }
      allEvents.forEach(event => {
          if (event.id != changedDateEvent.id) {            
              
            if (DateComparison.isInConflict(changedDateEvent,event)) {              
                changedDateEvent.dateIsValid = false;                         
                if (this.showErrorDiv == false) {
                  this.errorMessage = "There is already an event scheduled in this time: " + "\"" + event.title + "\"" + " (" + moment(event.start).format('DD/MM/YYYY') + " from " + moment(event.start).format('h:mma') +  " to " + moment(event.end).format('h:mma DD/MM/YYYY') + ")";                  
                  this.showErrorDiv = true;                  
                }                
            }
          }
      });
      if (this.showErrorDiv == false) {
        changedDateEvent.dateIsValid = true;
      }
    })

    this.modalRef = this.modal.open(this.modalContent,{ size: 'lg'});
  }

  // Adiciona um novo evento fazio no array de novos eventos, soh pra aparece no modal
  public createNewEventField(): void {   
    // Cria um evento vazio
    let event: CalendarEvent = {   
      id: -1,  
      title: '',
      start: startOfDay(this.viewDate), //Seta a data inicial pra o inicio do dia que o usuario selecionou ao clicar
      end: endOfDay(this.viewDate),     //Seta a data final pra o fim do dia que o usuario selecionou ao clicar
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      dateIsValid: true
    };
    
    // Se for o primeiro novo evento a ser adicionado
    if (this.newEvents == undefined) {
      this.newEvents = [event]; // Inicialize o array, soh com esse evento
    } else {
      // Adiciona o novo evento no inicio do array
      this.newEvents.unshift(event);    
    }
    // Avisa o calendar.component para atuaizar os componentes    
    this.refreshEmitter.emit();        
  }

  public deleteNewEvent(index): void {  
    this.newEvents.splice(index, 1);    
    this.refreshEmitter.emit();  
  }

  public deleteEventModal(index): void {    
    this.eventsToDisplay.splice(index, 1);    
    this.deleteEventEmitter.emit(index);    
  }

  public submitModal(): void {
    
    //  ADD AVENTS

    // Se tiver pelo menos um elemento no array de novos eventos
    if (this.newEvents != undefined && this.newEvents.length > 0) { 
      if (this.authService.isLoggedout()) {    
        this.addEventEmitter.emit(this.newEvents);
      }       
      // If is logged in
      //  ADD AVENTS to backend
      else { 
        this.calendarioService.addEvents(this.newEvents).subscribe(response => {          
          if (response['message'] == "OK") {   
              // Emitir esse array para que o calendario.component possa adiciona-lo a interface e atualizar
              this.addEventEmitter.emit(this.newEvents);
          } else {
            console.log(response['message']);
            this.errorMessage = response['message'];
            this.showErrorDiv = true;            
          }         
        });
      }    
    }  
      
    //  UPDATE EVENTS from backend
    if (this.authService.isAuthenticated()) {            
      let eventsToUpdate: CalendarEvent[] = [];
      
      for (let i = 0; i < this.eventsToDisplay.length; i++) {      
        if (JSON.stringify(this.eventsToDisplay[i]) != JSON.stringify(this.originalEventsToDisplay[i])) { 
          if (this.eventsToDisplay[i].dateIsValid) {
            eventsToUpdate.push(Object.assign({}, this.eventsToDisplay[i]));
          }
        }
      }
  
      if (eventsToUpdate.length > 0) {      
        this.calendarioService.updateEvents(eventsToUpdate).subscribe(response => {
          // Se der alguma coisa errada
          if (response['message'] != "OK") {
              this.eventsToDisplay = this.originalEventsToDisplay;  // Restaurar as configuracoes originais
              console.log(response['message']);          
              this.errorMessage = response['message'];
              this.showErrorDiv = true;              
          } 
          if (this.showErrorDiv == false) {
            this.closeModal();
          }  
        });
      }  else { // if  eventsToUpdate.length == 0
        if (this.showErrorDiv == false) {
         this.closeModal();
        }  
      }      
    }  else {      
        this.closeModal();
    }                   
  }

  public closeModal(): void {  
    for (let i = 0; i < this.eventsToDisplay.length; i++) {      
      if (JSON.stringify(this.eventsToDisplay[i]) != JSON.stringify(this.originalEventsToDisplay[i])) {      
        this.addEventEmitter.emit([]);
      }
    }
    this.modalRef.close();
  }
}
